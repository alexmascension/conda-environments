# Conda environments
This project simply collects all the environment files for conda that I will 
create for necessary reasons.

Each yaml file can be used as follows:
* To create the environment: `conda env create -f environment.yml`
* To activate the environment: `conda activate myenv`
* To remove the environment: `conda env remove --name myenv`

After a conda environment has been set, if the terminal is restored, the
environment may not be loaded by default and, instead, the base environment
is activated. 

If you want to activate this environment by default, write this line (or
override it if it has already been written) in your home `.bashrc` file:
`source activate myenv"`.
If you want to revert back to base by default, remove that line.

## How to do this
The running sequence is the following:
`conda env create > conda activate > bashrc > restart terminal` 

In some cases you may need to restart the terminal before activation,
then the sequence order is:
`conda env create > conda init bash > restart > conda activate myenv > bashrc > restart terminal`

## Running a notebook on Jupyter
It is possible that a notebook may not run properly because it is using another
environment (the base one possibly). 

Once you run Jupyter, check that the environment is the correct
either on the `Conda` tab in the jupyter notebook window, or check the
environment within the notebook at the top right corner, below `Logout` button. 

If this does not work, run this command on the console

`python -m ipykernel install --user --name MY-ENV --display-name "Jupyter (MY-ENV)"`

This command will set the new environment for Jupyter. .
