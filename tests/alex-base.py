# This is a test to import the main modules installed in conda
print('Importing numpy')
import numpy

print('Importing pandas')
import pandas

print('Importing matplotlib')
import matplotlib.pyplot as plt

print('Importing seaborn')
import seaborn as sns

print('Importing scipy')
import scipy

print('Importing sklearn')
import sklearn

print('Importing numba')
import numba

print('Importing dask')
import dask

print('Importing graphviz')
import graphviz

print('Importing scikit-image')
import skimage

print('Importing sympy')
import sympy

print('Importing plotly')
import plotly

import subprocess

try:
    p = subprocess.check_output("which salmon", shell=True)
    print("Salmon installed in " + p.decode('utf-8'))
except:
    raise('Salmon not installed')

try:
    p = subprocess.check_output("which STAR", shell=True)
    print("STAR installed in " + p.decode('utf-8'))
except:
    raise('STAR not installed')

try:
    p = subprocess.check_output("which fastp", shell=True)
    print("fastp installed in " + p.decode('utf-8'))
except:
    raise('fastp not installed')

try:
    p = subprocess.check_output("which umi_tools", shell=True)
    print("umi_tools installed in " + p.decode('utf-8'))
except:
    raise('umi_tools not installed')

try:
    p = subprocess.check_output("which featureCounts", shell=True)
    print("subread installed in " + p.decode('utf-8'))
except:
    raise('subread not installed')


print('Importing igraph')
import igraph

print('Importing louvain')
import louvain

try:
    p = subprocess.check_output("which parallel-fastq-dump", shell=True)
    print("parallel-fastq-dump installed in " + p.decode('utf-8'))
except:
    raise('parallel-fastq-dump not installed')

print('Importing biopython')
import Bio

print('Importing bigmpi4py')
import bigmpi4py

try:
    p = subprocess.check_output("which jupyter", shell=True)
    print("jupyter installed in " + p.decode('utf-8'))
except:
    raise('jupyter not installed')

print('Importing MulticoreTSNE')
import MulticoreTSNE

print('Importing vpolo')
import vpolo

print('Importing cigar')
import cigar

print('Importing Levenshtein')
import Levenshtein

print('Importing scanpy')
import scanpy

print('Importing leidenalg')
import leidenalg

print('Importing velocyto')
import velocyto

print('Importing more-itertools')
import more_itertools
